import sass from "rollup-plugin-sass";
import minify from "rollup-plugin-babel-minify";

export default {
	input: "src/widro-slider.js",
	output: [
		{
			file: "dist/widro-slider.js",
			format: "umd",
			name: "WidroSlider"
		},
		{
			file: "dist/widro-slider.mjs",
			format: "esm"
		}
	],
	plugins: [sass(), minify({ comments: false })]
};
