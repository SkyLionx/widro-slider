export as namespace WidroSlider;
export default WidroSlider;

declare class WidroSlider extends HTMLElement {
	static observedAttributes: string[];

	value: number;
	max: number;
	min: number;
	vertical: boolean;
	knobPosition: "inside" | "outside";
	knobSize: number;
	barSize: number;
	primaryColor: string;
	secondaryColor: string;
	borderRadius: number;

	connected: boolean;
	dragging: boolean;
	
	shadow: ShadowRoot;

	slider: HTMLDivElement;
	fill: HTMLDivElement;
	knob: HTMLDivElement;

	constructor();

	connectedCallback(): void;
	update(): void;
	attributeChangedCallback(name: string, oldValue: string, newValue: string): void;

	getValueFromMouse(event: Event): number;
	getPercFromValue(value: number): number;

	checkValues(): void;
}
