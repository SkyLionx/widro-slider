import cssStyle from "./slider.scss";

const DEFAULT_MIN_VALUE = 0;
const DEFAULT_MAX_VALUE = 100;
const DEFAULT_KNOBPOSITION_VALUE = "inside";

const template = document.createElement("template");
template.innerHTML = cssStyle;

class WidroSlider extends HTMLElement {
	constructor() {
		super();

		this.connected = false;
		this.dragging = false;

		/* Slider creation */
		this.shadow = this.attachShadow({ mode: "open" });

		// Apply external styles to the shadow DOM
		const style = document.createElement("style");
		style.appendChild(template.content.cloneNode(true));
		this.shadow.appendChild(style);

		this.slider = document.createElement("div");
		this.slider.classList.add("widro-slider");

		this.fill = document.createElement("div");
		this.fill.classList.add("widro-slider-fill");

		this.knob = document.createElement("div");
		this.knob.classList.add("widro-slider-knob");

		this.shadow.appendChild(this.slider);
		this.shadow.appendChild(this.fill);
		this.shadow.appendChild(this.knob);

		this.checkValues();

		/* Slider events */
		function handleDown(e) {
			// Drag started

			this.dragging = true;
			this.value = this.getValueFromMouse(e);
			let moved = false;
			this.dispatchEvent(new Event("changestart"));
			this.dispatchEvent(new Event("change"));
			this.update();

			const updateValue = function (e) {
				this.value = this.getValueFromMouse(e);
				moved = true;
				this.dispatchEvent(new Event("change"));
				this.update();
			}.bind(this);

			const updateValueAndDelete = function (e) {
				this.dragging = false;
				if (moved) updateValue(e);

				this.dispatchEvent(new Event("changeend"));

				document.removeEventListener("mousemove", updateValue);
				document.removeEventListener("touchmove", updateValue);

				document.removeEventListener("mouseup", updateValueAndDelete);
				document.removeEventListener("touchend", updateValueAndDelete);
				document.removeEventListener(
					"touchcancel",
					updateValueAndDelete
				);
			}.bind(this);

			document.addEventListener("mousemove", updateValue);
			document.addEventListener("touchmove", updateValue, {
				passive: true,
			});

			document.addEventListener("mouseup", updateValueAndDelete);
			document.addEventListener("touchend", updateValueAndDelete);
			document.addEventListener("touchcancel", updateValueAndDelete);
		}

		this.addEventListener("mousedown", handleDown);
		this.addEventListener("touchstart", handleDown, { passive: true });
	}

	/**
	 * Called each time this element is connected in the DOM
	 */
	connectedCallback() {
		this.connected = true;
		this.update();
	}

	getValueFromMouse(e) {
		const containerRect = this.getBoundingClientRect();
		const size = this.vertical ? containerRect.height : containerRect.width;
		const begin = this.vertical ? containerRect.bottom : containerRect.left;
		const x = e.clientX
			? e.clientX
			: e.touches.length > 0
			? e.touches[0].clientX
			: e.changedTouches[0].clientX;
		const y = e.clientY
			? e.clientY
			: e.touches.length > 0
			? e.touches[0].clientY
			: e.changedTouches[0].clientY;
		const realCoord = this.vertical ? begin - y : x - begin;
		const value =
			Math.abs(this.max - this.min) * (realCoord / size) + this.min;
		return clamp(value, this.min, this.max);
	}

	getPercFromValue(value) {
		const totalValues = Math.abs(this.max - this.min);
		return (Math.abs(value - this.min) / totalValues) * 100;
	}

	checkValues() {
		if (this.max < this.min)
			throw new Error("Max cannot be less than min!");
	}

	/**
	 * Update UI
	 */
	update() {
		const knobSize = this.knob.offsetWidth;
		if (knobSize == 0)
			console.warn(
				"Couldn't get the knob size, the slider could not function properly.\n" +
					"If the slider has the hidden attribute, please use 'visibility: hidden' instead."
			);
		const containerSize = this.vertical
			? this.offsetHeight
			: this.offsetWidth;
		const perc = this.getPercFromValue(this.value);
		let knobPos = "calc(" + perc + "% - " + knobSize / 2 + "px)";
		if (this.knobPosition == "inside") {
			const calculatedPos = (containerSize * perc) / 100;
			if (calculatedPos < knobSize / 2) knobPos = "0px";
			else if (calculatedPos > containerSize - knobSize / 2)
				knobPos = containerSize - knobSize + "px";
		}
		if (this.vertical) {
			this.fill.style.height = perc + "%";
			this.knob.style.bottom = knobPos;

			this.fill.style.width = "";
			this.knob.style.left = "";
		} else {
			this.fill.style.width = perc + "%";
			this.knob.style.left = knobPos;

			this.fill.style.height = "";
			this.knob.style.bottom = "";
		}
	}

	/**
	 * Called each time an attribute is changed
	 */
	attributeChangedCallback(name, oldValue, newValue) {
		this.checkValues();
		if (this.connected) this.update();
	}

	/**
	 * Method needed by attributeChangedCallback
	 */
	static get observedAttributes() {
		return ["min", "value", "max", "vertical", "knob-position"];
	}

	/* Setters & getters */

	set value(value) {
		this.setAttribute("value", clamp(value, this.min, this.max));
	}

	get value() {
		return this.hasAttribute("value")
			? parseFloat(this.getAttribute("value"))
			: (this.min + this.max) / 2;
	}

	set max(max) {
		this.value = clamp(this.value, this.min, max);
		this.setAttribute("max", max);
	}

	get max() {
		return this.hasAttribute("max")
			? parseFloat(this.getAttribute("max"))
			: DEFAULT_MAX_VALUE;
	}

	set min(min) {
		this.value = clamp(this.value, min, this.max);
		this.setAttribute("min", min);
	}

	get min() {
		return this.hasAttribute("min")
			? parseFloat(this.getAttribute("min"))
			: DEFAULT_MIN_VALUE;
	}

	set vertical(val) {
		if (val) this.setAttribute("vertical", "");
		else this.removeAttribute("vertical");
	}

	get vertical() {
		return this.hasAttribute("vertical");
	}

	set knobPosition(val) {
		if (val) this.setAttribute("knob-position", val);
		else this.removeAttribute("knob-position");
	}

	get knobPosition() {
		return this.hasAttribute("knob-position")
			? this.getAttribute("knob-position")
			: DEFAULT_KNOBPOSITION_VALUE;
	}

	set knobSize(val) {
		this.style.setProperty("--knob-size", val);
	}

	get knobSize() {
		return getComputedStyle(this).getPropertyValue("--knob-size");
	}

	set barSize(val) {
		this.style.setProperty("--bar-size", val);
	}

	get barSize() {
		return getComputedStyle(this).getPropertyValue("--bar-size");
	}

	set primaryColor(val) {
		this.style.setProperty("--primary-color", val);
	}

	get primaryColor() {
		return getComputedStyle(this).getPropertyValue("--primary-color");
	}

	set secondaryColor(val) {
		this.style.setProperty("--secondary-color", val);
	}

	get secondaryColor() {
		return getComputedStyle(this).getPropertyValue("--secondary-color");
	}

	set borderRadius(val) {
		this.style.setProperty("--border-radius", val);
	}

	get borderRadius() {
		return getComputedStyle(this).getPropertyValue("--border-radius");
	}
}

function clamp(value, min, max) {
	return Math.min(max, Math.max(value, min));
}

// Create custom elements
customElements.define("widro-slider", WidroSlider);

export default WidroSlider;
